// Soal No 1

var daftarHewan = ['2.Komodo', '5.Buaya', '3.Cicak', '4.Ular', '1.Tokek'];

// Jawaban No 1
var hewan = daftarHewan.sort();
daftarHewan.forEach(function (hewan) {
  console.log(hewan);
});

// soal 2
var data = { name : "Riky Suherman" , age : 23 , address : "Sukabumi" , hobby : "Badminton" }

// jawaban 2
function introduction(data){
var nama = data.name 
var umur = data.age
var alamat = data.address
var hobi = data.hobby
return "Nama saya " + nama + ", " + "umur saya " + umur + " tahun, " + "alamat saya di " + alamat + ", " + "dan saya punya hobby yaitu " + hobi
}
var perkenalan = introduction(data)
console.log(perkenalan)


// Jawaban No 3

function hitung_huruf_vokal(str) {
  var count = str.match(/[aiueo]/gi).length;
  return count;
}

var hitung1 = hitung_huruf_vokal('Muhammad');
var hitung2 = hitung_huruf_vokal('iqbal');
console.log(hitung1, hitung2); // 3 2

// Jawaban No 4
function deret(angka) {
  return angka * 2 - 2;
}

console.log(deret(0)); // -2
console.log(deret(1)); // 0
console.log(deret(2)); // 2
console.log(deret(3)); // 4
console.log(deret(5)); // 8
